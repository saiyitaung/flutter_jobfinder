import 'dart:ffi';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jobfinder/model/job_response.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final jobsTags = [
      "All Job",
      "Software",
      "Writer",
      "Design",
      "Finance",
      "Enginner"
    ];
    return Scaffold(
      body: BlocProvider(
        create: (context) =>
            JobBloc(JobState(), context.read<JobRespository>()),
        child: BlocConsumer<JobBloc, JobState>(
          listener: (context, state) {},
          builder: (context, state) {
            switch (state.state) {
              case DataState.init:
                return const Center(
                  child: Text("iniit"),
                );
              case DataState.loading:
                return const Center(
                  child: CircularProgressIndicator(),
                );
              case DataState.fail:
                return const Center(
                  child: Text("Fail"),
                );
              case DataState.success:
                return SafeArea(
                  child: RefreshIndicator(
                    onRefresh: () async {
                      context.read<JobBloc>().loadJob();
                    },
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Padding(
                                padding: EdgeInsets.only(left: 20),
                                child: Text(
                                  "Welcome!",
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(right: 10),
                                decoration: BoxDecoration(
                                  color: Colors.blue[100],
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                padding: const EdgeInsets.all(14),
                                child: Icon(
                                  Icons.notifications,
                                  color: Colors.blue[400],
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                  child: TextField(
                                decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    hintText: 'Search',
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(18),
                                    ),
                                    suffixIcon: const Icon(Icons.search)),
                              )),
                              const SizedBox(
                                width: 10,
                              ),
                              Container(
                                margin: const EdgeInsets.only(right: 10),
                                decoration: BoxDecoration(
                                  color: Colors.blue[100],
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                padding: const EdgeInsets.all(14),
                                child: Icon(
                                  Icons.filter_alt,
                                  color: Colors.blue[400],
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Tips for you",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text("see all"),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(horizontal: 10),
                            decoration: BoxDecoration(
                              color: Colors.blue[500],
                              borderRadius: BorderRadius.circular(16),
                            ),
                            height: 150,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Job recommandation",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text("see all"),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: 40,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              padding: const EdgeInsets.only(left: 10),
                              itemBuilder: (context, index) {
                                return Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(18),
                                    color: index == 0
                                        ? const Color.fromARGB(
                                            255,
                                            52,
                                            141,
                                            213,
                                          )
                                        : null,
                                    border: Border.all(
                                      width: 2,
                                      color: const Color.fromARGB(
                                        255,
                                        52,
                                        141,
                                        213,
                                      ),
                                    ),
                                  ),
                                  margin: const EdgeInsets.only(right: 10),
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  alignment: Alignment.center,
                                  child: Text(
                                    jobsTags[index],
                                    style: TextStyle(
                                        color:
                                            index == 0 ? Colors.white : null),
                                  ),
                                );
                              },
                              itemCount: jobsTags.length,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              final job = state.resp?.data?[index];
                              return JobCard(job: job);
                            },
                            itemCount: state.resp?.data?.length ?? 0,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
            }
          },
        ),
      ),
    );
  }
}

class JobCard extends StatelessWidget {
  final JobModel? job;
  const JobCard({super.key, required this.job});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: const Color(0xfff5f7ff),
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: FadeInImage.assetNetwork(
                    placeholder: "",
                    image: job?.companyProfile?.profile ?? '',
                    imageErrorBuilder: (context, error, stackTrace) =>
                        const Icon(
                      Icons.broken_image_rounded,
                      size: 60,
                    ),
                    placeholderErrorBuilder: (context, error, stackTrace) =>
                        const Icon(
                      Icons.downloading_rounded,
                      size: 60,
                    ),
                    width: 55,
                    height: 55,
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      job?.companyProfile?.company ?? '',
                      style: const TextStyle(
                          fontWeight: FontWeight.w500, color: Colors.black45),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      job?.title ?? 'Unknown',
                      maxLines: 1,
                      overflow: TextOverflow.fade,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ],
                )),
                const SizedBox(
                  width: 16,
                ),
                const Icon(
                  Icons.favorite,
                  size: 28,
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Row(
                        children: [
                          Icon(Icons.location_on_rounded),
                          SizedBox(
                            width: 8,
                          ),
                          Text("XYZ , USA ")
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 4),
                            decoration: BoxDecoration(
                                color: const Color(0xff333A73),
                                borderRadius: BorderRadius.circular(10)),
                            child: const Text(
                              "Software",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 13),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 4),
                            decoration: BoxDecoration(
                                color: const Color(0xff333A73),
                                borderRadius: BorderRadius.circular(10)),
                            child: const Text(
                              "Remote",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 13),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Text(
                  Random().nextInt(2) == 0 ? "Nego" : "\$2,200",
                  style: const TextStyle(
                      fontWeight: FontWeight.w500, fontSize: 18),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class JobRespository {
  Future<JobResponse> getJobs() async {
    final resp =
        await Dio().get('https://405f-104-28-219-153.ngrok-free.app/api/jobs');
    return JobResponse.fromJson(resp.data);
  }
}

enum DataState { init, loading, success, fail }

class JobState {
  DataState state;
  JobResponse? resp;
  JobState({this.state = DataState.init, this.resp});
}

class JobBloc extends Cubit<JobState> {
  JobBloc(super.initialState, this.repo) {
    loadJob();
  }
  JobRespository repo;

  Future<void> loadJob() async {
    emit(JobState(state: DataState.loading));
    try {
      final jobs = await repo.getJobs();
      emit(JobState(state: DataState.success, resp: jobs));
    } catch (e) {
      emit(JobState(state: DataState.fail));
    }
  }
}
