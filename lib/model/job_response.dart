// To parse this JSON data, do
//
//     final jobResponse = jobResponseFromJson(jsonString);

import 'dart:convert';

JobResponse jobResponseFromJson(String str) =>
    JobResponse.fromJson(json.decode(str));

String jobResponseToJson(JobResponse data) => json.encode(data.toJson());

class JobResponse {
  List<JobModel>? data;
  Links? links;
  Meta? meta;

  JobResponse({
    this.data,
    this.links,
    this.meta,
  });

  factory JobResponse.fromJson(Map<String, dynamic> json) => JobResponse(
        data: json["data"] == null
            ? []
            : List<JobModel>.from(
                json["data"]!.map((x) => JobModel.fromJson(x))),
        links: json["links"] == null ? null : Links.fromJson(json["links"]),
        meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
        "links": links?.toJson(),
        "meta": meta?.toJson(),
      };
}

class JobModel {
  int? id;
  String? title;
  List<String>? description;
  List<String>? requirement;
  List<String>? tags;
  DateTime? created;
  CompanyProfile? companyProfile;

  JobModel({
    this.id,
    this.title,
    this.description,
    this.requirement,
    this.tags,
    this.created,
    this.companyProfile,
  });

  factory JobModel.fromJson(Map<String, dynamic> json) => JobModel(
        id: json["id"],
        title: json["title"],
        description: json["description"] == null
            ? []
            : List<String>.from(json["description"]!.map((x) => x)),
        requirement: json["requirement"] == null
            ? []
            : List<String>.from(json["requirement"]!.map((x) => x)),
        tags: json["tags"] == null
            ? []
            : List<String>.from(json["tags"]!.map((x) => x)),
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
        companyProfile: json["company_profile"] == null
            ? null
            : CompanyProfile.fromJson(json["company_profile"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description == null
            ? []
            : List<dynamic>.from(description!.map((x) => x)),
        "requirement": requirement == null
            ? []
            : List<dynamic>.from(requirement!.map((x) => x)),
        "tags": tags == null ? [] : List<dynamic>.from(tags!.map((x) => x)),
        "created": created?.toIso8601String(),
        "company_profile": companyProfile?.toJson(),
      };
}

class CompanyProfile {
  String? company;
  String? profile;
  String? email;

  CompanyProfile({
    this.company,
    this.profile,
    this.email,
  });

  factory CompanyProfile.fromJson(Map<String, dynamic> json) => CompanyProfile(
        company: json["company"],
        profile: json["profile"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "company": company,
        "profile": profile,
        "email": email,
      };
}

class Links {
  String? first;
  String? last;
  dynamic prev;
  dynamic next;

  Links({
    this.first,
    this.last,
    this.prev,
    this.next,
  });

  factory Links.fromJson(Map<String, dynamic> json) => Links(
        first: json["first"],
        last: json["last"],
        prev: json["prev"],
        next: json["next"],
      );

  Map<String, dynamic> toJson() => {
        "first": first,
        "last": last,
        "prev": prev,
        "next": next,
      };
}

class Meta {
  int? currentPage;
  int? from;
  int? lastPage;
  List<Link>? links;
  String? path;
  int? perPage;
  int? to;
  int? total;

  Meta({
    this.currentPage,
    this.from,
    this.lastPage,
    this.links,
    this.path,
    this.perPage,
    this.to,
    this.total,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"],
        from: json["from"],
        lastPage: json["last_page"],
        links: json["links"] == null
            ? []
            : List<Link>.from(json["links"]!.map((x) => Link.fromJson(x))),
        path: json["path"],
        perPage: json["per_page"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "from": from,
        "last_page": lastPage,
        "links": links == null
            ? []
            : List<dynamic>.from(links!.map((x) => x.toJson())),
        "path": path,
        "per_page": perPage,
        "to": to,
        "total": total,
      };
}

class Link {
  String? url;
  String? label;
  bool? active;

  Link({
    this.url,
    this.label,
    this.active,
  });

  factory Link.fromJson(Map<String, dynamic> json) => Link(
        url: json["url"],
        label: json["label"],
        active: json["active"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
        "label": label,
        "active": active,
      };
}
